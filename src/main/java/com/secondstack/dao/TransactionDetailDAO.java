/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.dao;

import com.secondstack.model.TransactionDetail;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author jasoet
 */
public interface TransactionDetailDAO {

    public List<TransactionDetail> getAll() throws SQLException;

    public TransactionDetail find(int id) throws SQLException;
    
    public int insert(TransactionDetail d) throws SQLException;

    public int delete(int id) throws SQLException;

    public int update(int id, TransactionDetail newTransactionDetail) throws SQLException;
}
