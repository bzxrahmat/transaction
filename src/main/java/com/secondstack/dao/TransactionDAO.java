/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.dao;

import com.secondstack.model.Transaction;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author jasoet
 */
public interface TransactionDAO {

    public List<Transaction> getAll() throws SQLException;

    public Transaction find(int id) throws SQLException;

    public int insert(Transaction d) throws SQLException;

    public int delete(int id) throws SQLException;

    public int update(int id, Transaction newTransaction) throws SQLException;
}
