package com.secondstack.dao.impl;

import com.secondstack.dao.ProductDAO;
import com.secondstack.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDAOImpl implements ProductDAO {

    private Connection connection;

    public ProductDAOImpl(Connection connection) {
        this.connection = connection;
    }

    public List<Product> getAll() throws SQLException {
        String sql = "SELECT d.product_id AS id,d.name,d.description FROM product d";
        PreparedStatement ps = connection.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        List<Product> data = new ArrayList<Product>();

        while (rs.next()) {
            Product d = new Product();
            d.setId(rs.getInt("id"));
            d.setName(rs.getString("name"));
            d.setDescription(rs.getString("description"));
            data.add(d);
        }

        return data;

    }

    public List<Product> getByName(String name) throws SQLException {
        String sql = "SELECT d.product_id AS id,d.name,d.description FROM product d WHERE d.name LIKE ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "%" + name + "%");

        ResultSet rs = ps.executeQuery();
        List<Product> data = new ArrayList<Product>();

        while (rs.next()) {
            Product d = new Product();
            d.setId(rs.getInt("id"));
            d.setName(rs.getString("name"));
            d.setDescription(rs.getString("description"));
            data.add(d);
        }

        return data;
    }

    public int insert(Product d) throws SQLException {
        String sql = "INSERT INTO product (name,description) VALUES (?,?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, d.getName());
        ps.setString(2, d.getDescription());
        return ps.executeUpdate();
    }

    public int delete(int id) throws SQLException {
        String sql = "DELETE FROM product WHERE product_id=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, id);
        return ps.executeUpdate();
    }

    public int update(int id, Product newProduct) throws SQLException {
        String sql = "UPDATE product SET name=?,description=? WHERE product_id=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, newProduct.getName());
        ps.setString(2, newProduct.getDescription());
        ps.setInt(3, id);
        return ps.executeUpdate();
    }

    public Product find(int id) throws SQLException {
        String sql = "SELECT d.product_id AS id,d.name,d.description FROM product d WHERE d.product_id = ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, id);

        ResultSet rs = ps.executeQuery();
        Product o = null;
        if (rs.next()) {
            o = new Product();
            o.setId(rs.getInt("id"));
            o.setName(rs.getString("name"));
            o.setDescription(rs.getString("description"));
        }

        return o;

    }
}
