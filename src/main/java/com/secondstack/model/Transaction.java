/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.model;

import java.util.Date;

/**
 *
 * @author jasoet
 */
public class Transaction {
    
    private int id;
    private Date time;
    private String customerName;
    private String customerAddress;

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    
    
}
