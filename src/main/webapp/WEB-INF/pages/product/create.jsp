
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include  page="/WEB-INF/pages/shared/head.jsp"  >
    <jsp:param name="title" value="New Product"/>
</jsp:include>

<form action="<c:url value='/product'/>" method="post">
    <div>
        Name : <br/>
        <input type="text" name="name"/>
    </div>
    <div>
        Description: <br/>
        <textarea name="description"></textarea>
    </div>
    <div>
        <input type="submit" value="Save"/>
    </div>
</form>


<jsp:include page="/WEB-INF/pages/shared/foot.jsp"/>
