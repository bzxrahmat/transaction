
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include  page="/WEB-INF/pages/shared/head.jsp"  >
    <jsp:param name="title" value="List Products"/>
</jsp:include>

<table border="1">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${data}" var="o">
            <tr>
                <td>${o.id}</td>
                <td>${o.name}</td>
                <td>${o.description}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<jsp:include page="/WEB-INF/pages/shared/foot.jsp"  />
